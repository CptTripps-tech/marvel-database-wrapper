import hashlib
import time
import requests
import config

# Constructing the Hash
m = hashlib.md5()

ts = str(time.time())
ts_bytes = bytes(ts, 'utf-8')
m.update(ts_bytes)
m.update(config.private_key)
m.update(config.public_key)

hash = m.hexdigest()

base_url = "https://gateway.marvel.com"
api_key = "7f5dbfc0f98aef80e25297d12a0369ef"
api_parameters = "&ts=" + ts + "&apikey=" + api_key + "&hash=" + hash


def findHeroByName(str):
    query = "/v1/public/characters?name=" + str
    query_url = base_url + query + api_parameters
    return requests.get(query_url).json()


def findComicsByHero(str):
    result = findHeroByName(str)
    characterid = ''
    d = result['data']['results']
    for i in d:
        characterid = (i['id'])
    query = "/v1/public/characters/" + f'{characterid}' + "?"
    query_url = base_url + query + api_parameters
    return requests.get(query_url).json()