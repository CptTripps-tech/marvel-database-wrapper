# Marvel Database Wrapper
A Database-Wrapper written in Python to access the Marvel-Comics Database via the official API.

More Infos about the API under: https://developer.marvel.com/documentation/generalinfo
