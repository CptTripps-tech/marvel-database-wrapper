import unittest
import DataBaseHandler


class FindHeroByNameTest(unittest.TestCase):
    def test(self):
        result = DataBaseHandler.findHeroByName("Spider-Man")
        self.assertEqual(result['code'], 200)


class FindComicsByHeroTest(unittest.TestCase):
    def test(self):
        result = DataBaseHandler.findComicsByHero("Hulk")
        self.assertEqual(result['code'], 200)
